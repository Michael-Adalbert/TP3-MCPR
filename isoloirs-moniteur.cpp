#include "isoloirs-moniteur.h"
#include <string.h>

IsoloirsMoniteur::IsoloirsMoniteur(int _nb_isoloirs)
{
    nb_isoloirs = _nb_isoloirs;
    index_isoloirs_arrivee = 0;
    index_isoloirs_depart = 0;
    for (int i = 0; i < nb_isoloirs; ++i)
    {
        buffer[i] = false;
    }
    nb_isoloirs_plein = 0;
    isoloirs_vide_prio_normal = newCondition();
    isoloirs_vide_prio_haute = newCondition();
    isoloirs_plein = newCondition();
}

void IsoloirsMoniteur::acceder()
{
    buffer[index_isoloirs_arrivee] = true;
    index_isoloirs_arrivee = (index_isoloirs_arrivee + 1) % nb_isoloirs;
}

void IsoloirsMoniteur::partir()
{
    buffer[index_isoloirs_depart] = false;
    index_isoloirs_depart = (index_isoloirs_depart + 1) % nb_isoloirs;
}

void IsoloirsMoniteur::acceder(int r, int prio)
{
    enter();

    if (nb_isoloirs_plein == nb_isoloirs)
    {
        if (prio % 2 == 0)
        {
            isoloirs_vide_prio_haute->wait();
        }
        else
        {
            isoloirs_vide_prio_normal->wait();
        }
    }

    acceder();

    nb_isoloirs_plein++;

    printf("\t\tVoteur %d | Prio %d : Entre dans l'isoloirs \n", r, prio);

    leave();
}

void IsoloirsMoniteur::partir(int r)
{
    enter();

    partir();

    nb_isoloirs_plein--;

    printf("\t\tVoteur %d : Part de l'isoloirs \n", r);

    if (isoloirs_vide_prio_haute->length() > 0)
    {
        isoloirs_vide_prio_haute->signal();
    }
    else
    {
        isoloirs_vide_prio_normal->signal();
    }

    leave();
}