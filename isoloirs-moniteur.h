#ifndef __ISOLOIRS_MONITEUR_H__
#define __ISOLOIRS_MONITEUR_H__

#include "MONITEURS_HOARE/HoareMonitor.h"
#include "MONITEURS_HOARE/HoareCondition.h"

#define NB_MAX_ISOLOIRS 10

class IsoloirsMoniteur : public HoareMonitor
{
private:
    bool buffer[NB_MAX_ISOLOIRS];
    int index_isoloirs_arrivee;
    int index_isoloirs_depart;
    int nb_isoloirs;

    HoareCondition *isoloirs_plein, *isoloirs_vide_prio_normal,*isoloirs_vide_prio_haute;
    int nb_isoloirs_plein;
    void acceder();
    void partir();

public:
    IsoloirsMoniteur(int nb_isoloirs);
    void acceder(int r, int prio);
    void partir(int r);
};

#endif